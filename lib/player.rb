class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play

    input = gets.chomp.split(",").map(&:to_i)
    if input.length < 2
      puts "Try again, use '\X,X'\ denotation"
      get_play
    end
    input
  end

end
